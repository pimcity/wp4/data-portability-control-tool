Data Portability Control (DPC)
==============================

# Intro

The Data Portability Control (DPC) allows users to migrate their data to new platforms, in a privacy-preserving fashion. More specifically, it incorporates the necessary tools to import data from multiple platforms (through the available Data Sources), process the data to remove sensitive information (through the Data Transformation Engine), and outport into other platforms (through the Data Export module). The tool does not provide a dedicated UI to the users. Instead, it provides an interface in a form of a generic Control API for controlling all operations from other modules of the PIM system (e.g., the User Dashboard). The figure below depicts the DPC architecture.

![System Design](docs/system-design.png)



# Installation

The DPC tool is a Python 3 (minimum Python 3.7) system that requires the following libraries:

```
flask
connexion
pymongo
requests
pyjwt
python-keycloak
```

You can execute the following command to install these dependencies automatically:
```
$ pip install -r requirements.txt 
```

An instance of [MongoDB](https://www.mongodb.com) v4.4+ should also be installed and running.



# Usage Examples

To execute the DPC tool in development mode, please use the following command:

```
$ python app.py -dev

```

Below you can find a list of all available (optional) arguments:

```
usage: app.py [-h] [-dev] [-host HOST] [-port PORT] [-dbu DB-URL] [-cdb]

Data Portability Control (DPC) Tool operations.

optional arguments:
  -h, --help            show this help message and exit
  -dev, --development   Run DPC in development (debug) mode without KeyClock authentication.
  -host HOST, --host HOST
                        Host that DPC will run (when in development mode).
  -port PORT, --port PORT
                        Port that DPC will run (when in development mode).
  -dbu DB-URL, --db-url DB-URL
                        MongoDB url that will be used.
  -cdb, --clean-db      Clean DB before running the tool.
```

The DPC tool does not have a dedicated UI for the users. Instead, it can be controlled using its Control API (a typical REST API) shown below:

![Control API](docs/api.jpg)

Assuming the DPC tool is installed and running, you can interact with its Control API for creating, configuring, and operating modules (i.e., Data Sources, Data Transformations and Data Exports). Swagger provides an interactive tool to experiment with the tool's endpoints and can be accessed from http://127.0.0.1:81/ui.


## Install a Data Source

First read the list of all installed data sources, using the relevant GET request (should be empty initially):

```
$ curl "http://127.0.0.1:81/datasources/"
```

Make sure that a data source class is available under the `datasources` folder (e.g., `datasource_truelayer`).

Now, install an instance of this class, using the relevant POST request:

```
$ curl -X "POST" "http://127.0.0.1:81/datasources/" \
     -H 'Content-Type: application/json; charset=utf-8' \
     -d $'{
  "class": "datasource-truelayer",
  "name": "Test",
  "manifest-version": 1,
  "type": "datasources"
}'

```

You Data Source is now installed and run within the DPC too. To verify this, repeat the previous GET request to see its status:

```
$ curl "http://127.0.0.1:81/datasources/"

```

When installed, a module (in this case the Data Source) starts in a deactivated state (enabled is false). To enable it, send a PATCH request on the `enabled` property:

```
curl -X "PATCH" "http://127.0.0.1:81/datasources/<datasource-id>/" \
     -H 'Content-Type: application/json; charset=utf-8' \
     -d $'{
  "enabled": true
}'

```

Similar approach can be followed for installing a Data Transformation or a Data Export module. For more information, please refer to the Control API mentioned above.



# Changes

### v0.0.2 (31/01/2022)
- Replace `flask-restful` with `connexion` framework
- Add Authentication using KeyCloak
- Add implementation for EasyPIMS demonstration

### v0.0.1 (10/06/2021)
- first public release of the DPC tool


# License

The DPC Tool is distributed under AGPL-3.0-only, see the [LICENSE](LICENSE.txt) file.

Copyright (C) 2021 Telefonica I+D - Kleomenis Katevas, Panagiotis Papadopoulos, Ioannis Arapakis, Nicolas Kourtelis.
