#! /usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from typing import Callable
import re


class DataMasking:
    config = {}

    def __init__(self, name, configuration):
        if configuration is None:
            raise Exception("No configuration found:", sys.exc_info()[0])
        self.regex = None
        self.sub = "*"
        if "columns" in configuration:
            self.whatToHide = configuration["columns"]
        if "regex" in configuration:
            self.regex = configuration["regex"]
        if "sub" in configuration:
            self.sub = configuration["sub"]

    def transform(self, data):
        if self.whatToHide is None:
            print("No column selected to anonymize")
            return data
        for elem in data:
            for k, v in elem.items():
                if k in self.whatToHide:
                    if self.regex is None:
                        re.sub('[^{3}]', '*', elem[k])
                    else:
                        elem[k] = re.sub(self.regex, self.sub, elem[k])
        return data
