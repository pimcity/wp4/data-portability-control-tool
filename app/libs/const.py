import os

UPLOAD_FOLDER = '../uploads/'
MAX_CONTENT_LENGTH = 50 * 1000 * 1000  # SET MAX UPLOAD SIZE TO 16 MB
ALLOWED_EXTENSIONS = {'json'}
DATASRC_PUSH = "datasource-push"
DATASRC_FB = "datasource-facebook"
SERVER_KEY = os.getenv('SECRET_KEY', "\xf9'\xe4p(\xa9\x12\x1a!\x94\x8d\x1c\x99l\xc7\xb7e\xc7c\x86\x02MJ\xa0")
