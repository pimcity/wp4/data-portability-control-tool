import pymongo
import json

from bson import json_util, ObjectId


class DB:

    def __init__(self, url=None, name='dbctool'):

        # default local url
        if not url:
            url = "mongodb://localhost:27017/"

        self.client = pymongo.MongoClient(url)
        self.name = name
        self.db = self.init_db(name)

    def is_connected(self):
        try:
            # test connection
            _ = self.client.server_info()
            return True

        except pymongo.errors.ServerSelectionTimeoutError as err:
            print(err)
            return False

    def init_db(self, name):
        # init db
        db = self.client[name]

        # init documents
        db.datasources = []
        db.transformations = []
        db.exports = []
        db.clients = []
        # return
        return db

    def __render_to_json(self, data):
        if data is None:
            return None
        if isinstance(data, pymongo.cursor.Cursor):
            items = list(data)
            for item in items:
                # fix _id
                item['_id'] = str(item['_id'])
            data = items
        else:
            # fix _id
            data['_id'] = str(data['_id'])

        return json.loads(json_util.dumps(data))

    def clean(self):
        self.client.drop_database(self.name)
        self.db = self.init_db(self.name)

    def get_one(self, name, entry_id):
        entry = self.db[name].find_one({"_id": ObjectId(entry_id)})
        return self.__render_to_json(entry)

    def get_name(self, name, entry_name):
        entry = self.db[name].find_one({"name": entry_name})
        return self.__render_to_json(entry)

    def get_all(self, name):
        data = self.db[name].find()
        return self.__render_to_json(data)

    def create_entries(self, name, data):
        entry = self.db[name].insert_many(data)
        return entry.inserted_ids

    def create_entry(self, name, data):
        entry = self.db[name].insert_one(data)
        return entry.inserted_id

    def update_one(self, name, entry_id, data):
        return self.db[name].update_one({"_id": ObjectId(entry_id)}, data)

    def replace_one(self, name, entry_id, data):
        return self.db[name].replace_one({"_id": ObjectId(entry_id)}, data)

    def delete_one(self, name, entry_id):
        return self.db[name].delete_one({"_id": ObjectId(entry_id)})

    def delete_all(self, name):
        return self.db[name].delete_many({})