#! /usr/bin/env python

from libs.utilities import get_type, setup_db_access, load_manifests, load_module, new_entry_checks


def get_operation(url, db):
    typeEntry = get_type(url)
    return db.get_all(typeEntry), 200


def post_operation(request, db):
    typeEntry = get_type(request.base_url)
    data = request.get_json(force=True)
    if db is None:
        db = setup_db_access()
    data = request.get_json(force=True)
    ret1, ret2 = new_entry_checks(data, typeEntry, db)
    if ret1 is None:
        return (None, ret2)
    exists = db.get_name(typeEntry, data.get("name"))
    if exists is not None:
        return (None, "An entry with the same name already exists (id:" + exists['_id'] + ")")
    newId = db.create_entry(typeEntry, ret1)
    return load_module(ret1), str(newId)


def patch_operation(request, entry_id, db): #, toRunIfEnabled=None):
    # first get entry
    typeEntry = get_type(request.base_url)
    entry = db.get_one(typeEntry, entry_id)
    if entry is None:
        return entry
    data = request.get_json(force=True)
    if data.get('manifest-version') is not None and data.get('manifest-version') != 1:
        return (None, "Only modules with manifest version of '1' are supported.")

    if data.get('type') is not None and data.get('type') != typeEntry:
        return (None, "invalid " + data.get('type') + " type.")

    if data.get('name') is not None and entry['name'] != data.get('name'):
        entry = db.get_name(typeEntry, data.get('name'))
        if entry is not None:
            return (None, "A different entry with the same name already exists")
    # update it
    db.update_one(typeEntry, entry_id, {'$set': data})
    module = load_module(db.get_one(typeEntry, entry_id))
    #if toRunIfEnabled is not None and data.get('enabled'):
    #    toRunIfEnabled(db, module, entry_id)
    return module, entry_id


def get_one_operation(url, entry_id, db):
    typeResource = get_type(url)
    return db.get_one(typeResource, entry_id)


def delete_operation(url, entry_id, db):
    # first get entry
    typeEntry = get_type(url)
    entry = db.get_one(typeEntry, entry_id)
    if entry is None:
        return None
    # delete it
    return db.delete_one(typeEntry, entry_id)


def put_operation(request, entry_id, db, toRunIfEnabled=None):
    # first get entry
    typeEntry = get_type(request.base_url)
    entry = db.get_one(typeEntry, entry_id)
    if entry is None:
        return None, "Entry with id: "+entry_id+" was not found"
    data = request.get_json(force=True)
    ret1, ret2 = new_entry_checks(data, typeEntry, db)
    if ret1 is None:
        return (None, ret2)
    if entry['name'] != data.get('name'):
        entry = db.get_name(typeEntry, data.get('name'))
        if entry is not None:
            return (None, "A different entry with the same name already exists")

    # update it
    db.replace_one(typeEntry, entry_id, ret1)
    module = load_module(db.get_one(typeEntry, entry_id))
    if toRunIfEnabled is not None and data.get('enabled'):
        toRunIfEnabled(db, module, entry_id)
    return module, entry_id
