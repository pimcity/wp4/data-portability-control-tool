#! /usr/bin/env python

import sys
import os
import json
import importlib
import pathlib
from libs.const import ALLOWED_EXTENSIONS
from libs.db import DB
from glob import glob
# from urllib.parse import urlparse


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def create_folder(path):
    if not pathlib.Path(path).exists():
        os.makedirs(path)


def get_type(url):
    if "datasources" in url:
        return "datasources"
    elif "datatransformations" in url:
        return "datatransformations"
    elif "dataexports" in url:
        return "dataexports"
    else:
        sys.exit("Something wrong happened when tokenizing url path")


# init connection and db
def setup_db_access(url=None, name='dbctool'):
    db = DB(url, name)
    if not db.is_connected():
        sys.exit("db connection error")
    return db


def load_manifests(resource):
    manifests = {}
    # get subdirs at path resource/*
    dirs = glob("./" + os.path.join(resource + "/*"))
    for path in dirs:
        # load manifest
        manifest = os.path.join(path, 'manifest.json')
        with open(manifest) as json_file:
            data = json.load(json_file)
            resource_type = data["type"]
            resource_class = data["class"]
            manifests[resource_class] = data
            print(f"> Loading {resource_type} module: {resource_class}.")
    return manifests


def load_module(entry):
    # get attributes
    resource_name = entry['name']
    type_name = entry['type']
    module = entry['module']
    class_name = entry['class-name']
    configuration = entry['configuration']
    # dynamicaly create class instance
    mod = importlib.import_module(f"{type_name}.{module}")
    class_ = getattr(mod, class_name)
    instance = class_(resource_name, configuration)
    return instance


def load_entries(db, typeEntry):
    res = {}
    for entry in db.get_all(typeEntry):
        # load entry
        module = load_module(entry)
        resource_name = entry['name']
        res[resource_name] = module
    return res


def new_entry_checks(data, typeEntry, db):
    manifest_version = data.get('manifest-version')
    if manifest_version is None:
        return (None, "Manifest version is missing.")

    if manifest_version != 1:
        return (None, "Only modules with manifest version of '1' are supported.")
    # check name
    resource_name = data.get('name')
    if resource_name is None:
        return (None, typeEntry + " name is missing.")
    # check type (should always be 'data-source')
    resource_type = data.get('type')
    if resource_type is None:
        return (None, typeEntry + " type is missing.")
    print(typeEntry, resource_type)
    if resource_type != typeEntry:
        return (None, resource_type + " type is invalid.")
    # check class
    resource_class = data.get('class')
    if resource_class is None:
        return (None, typeEntry + " class is missing.")

    manifestsAll = load_manifests(typeEntry)
    if len(manifestsAll) == 0:
        return (None, "There are no manifests loaded.")
    manifest = manifestsAll[resource_class]
    # create entry on db
    entry = {
        "name": resource_name,
        "type": manifest.get('type'),
        "class": manifest.get('class'),
        "description": manifest.get('description'),
        "module": manifest.get('module'),
        "class-name": manifest.get('class-name'),
        "configuration": manifest.get('configuration', {}),
        "enabled": False
    }
    return (entry, "Done")


def toJSONfile(filename, data):
    with open(filename, "w") as fw:
        json.dump(data, fw)
