#! /usr/bin/env python

import sys
import argparse
import connexion
from libs.utilities import setup_db_access
from connexion.resolver import MethodViewResolver

app = connexion.FlaskApp(__name__, specification_dir="./openapi/", options={"swagger_ui": True})


def main(args):
    # get arguments
    development = args['development']
    port = args['port']
    host = args['host']

    if development:
        yaml = "data-portability-control-tool-dev.yaml"
    else:
        yaml = "data-portability-control-tool.yaml"

    app.add_api(
        yaml,
        arguments={"title": "PIMCity Data Portability Control"},
        resolver=MethodViewResolver('api'),
        strict_validation=True, validate_responses=True
    )

    # setup db access
    db = setup_db_access()

    # clean db first if requested
    if args['clean-db']:
        print("> Cleaning the DB..")
        db.clean()

    app.run(debug=development, port=port, host=host)


# argument parser
def __parse_arguments(args):
    parser = argparse.ArgumentParser(description="Data Portability Control (DPC) Tool operations.")

    parser.add_argument('-dev', '--development', dest='development', action='store_true',
                        help='Run DPC in development (debug) mode without KeyClock authentication.')

    parser.add_argument('-host', '--host', dest='host', type=str, default='0.0.0.0',
                        help='Host that DPC will run (when in development mode).')

    parser.add_argument('-port', '--port', dest='port', type=int, default=81,
                        help='Port that DPC will run (when in development mode).')

    parser.add_argument('-dbu', '--db-url', dest='db-url', type=str, default='mongodb://localhost:27017/',
                        help='MongoDB url that will be used.')

    parser.add_argument('-cdb', '--clean-db', dest='clean-db', action='store_true',
                        help='Clean DB before running the tool.')

    parsed = vars(parser.parse_args(args))
    return parsed


##################################################################
# MAIN
##################################################################
if __name__ == '__main__':

    # parse args
    args = __parse_arguments(sys.argv[1:])

    # run main
    main(args)
