import sys
from libs.utilities import load_module


class PDSExporter:

    def __init__(self, name, configuration):
        if configuration is None:
            raise Exception("Wrong type of config file parsed:", sys.exc_info()[0])

        print(configuration)
        if "datasourceIDs" in configuration:
            self.datasourceIDs = configuration['datasourceIDs']
        if "datastransformationIDs" in configuration:
            self.datastransformationIDs = configuration['datastransformationIDs']

    def export(self, db):
        # run datasource and datatrasnform modules
        data = self.__loadAllData(db)
        self.__processData(db, data)

        # TODO: Send 'data' to PDS

        return {"message": "ok"}

    # proceeds and transform loaded data
    def __processData(self, db, data):
        for tid in self.datastransformationIDs:
            tran_entry = db.get_one('datatransformations', tid)
            if tran_entry is None:
                raise Exception("No datatransformation entry found with id: " + tid)
            if not tran_entry['enabled']:
                raise Exception("Datatransformation entry (id: " + tid + ") is not enabled")
            datatran_module = load_module(tran_entry)
            for did, values in data.items():
                datatran_module.transform(values)

    # retrieve data of all enabled datasources
    def __loadAllData(self, db):
        data = {}
        for did in self.datasourceIDs:
            src_entry = db.get_one('datasources', did)
            if src_entry is None:
                raise Exception("No datasource entry found with id: " + did)
            if not src_entry['enabled']:
                raise Exception("Datasource entry (id:" + did + ") is not enabled")
            datasrc_module = load_module(src_entry)
            data[did] = db.get_all(did)
        return data
