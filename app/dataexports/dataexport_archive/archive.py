import json
import pathlib
import sys
import argparse
from libs.utilities import load_module, create_folder

class ArchiveExporter:

    def __init__(self, name, configuration):
        if configuration is None:
            raise Exception("Wrong type of config file parsed:", sys.exc_info()[0])  
        # check for writable path
    #    self.outFile = self.__writeable_path(configuration['outputFile'].split("/")[-1])
        print(configuration)
        if "datasourceIDs" in configuration:
            self.datasourceIDs = configuration['datasourceIDs']
        if "datastransformationIDs" in configuration:
            self.datastransformationIDs = configuration['datastransformationIDs']

    def export(self, db):
        # run datasource and datatrasnform modules
        data = self.__loadAllData(db)
        self.__processData(db, data)
        return data

    # proceeds and transform loaded data
    def __processData(self, db, data):
        for tid in self.datastransformationIDs:
            tran_entry = db.get_one('datatransformations', tid)
            if tran_entry is None:
                raise Exception("No datatransformation entry found with id: " + tid)
            if not tran_entry['enabled']:
                raise Exception("Datatransformation entry (id: " + tid + ") is not enabled")
            datatran_module = load_module(tran_entry)
            for did, values in data.items():
                datatran_module.transform(values)

    # retrieve data of all enabled datasources
    def __loadAllData(self, db):
        data = {}
        for did in self.datasourceIDs:
            src_entry = db.get_one('datasources', did)
            if src_entry is None:
                raise Exception("No datasource entry found with id: " + did)
            if not src_entry['enabled']:
                raise Exception("Datasource entry (id:" + did + ") is not enabled")
            datasrc_module = load_module(src_entry)
            data[did] = db.get_all(did)
        return data

    # check if there is a writeable path
    def __writeable_path(self, possible_path):
        if possible_path is None:
            return possible_path
        create_folder(DOWNLOAD_FOLDER)
        possible_path = DOWNLOAD_FOLDER + possible_path
        tested_path = pathlib.Path(possible_path)
        if tested_path.exists():
            msg = f"cannot write to {possible_path}, file already exists"
            raise argparse.ArgumentTypeError(msg)
        target_dir = tested_path.parent
        if not target_dir.is_dir():
            msg = f"Invalid path to write to, {target_dir} is not a directory"
            raise argparse.ArgumentTypeError(msg)
        return tested_path