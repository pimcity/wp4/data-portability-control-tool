#! /usr/bin/env python

from flask import request, abort
from flask.views import MethodView
from connexion import NoContent
import libs.common_ops as ops
from libs.utilities import setup_db_access, get_type, load_module

db = None


class DataexportDownload():
    def download(request, dataexport_id):
        global db
        typeEntry = get_type(request.base_url)
        _init()
        dataexp_entry = db.get_one(typeEntry, dataexport_id)
        if dataexp_entry is None or not dataexp_entry.get('enabled') or dataexp_entry.get('class') != "dataexport-archive":
            abort(400, "Requested dataexport entry does not exist or not enabled")
        else:
            module = load_module(dataexp_entry)
            res = module.export(db)
        return res, 200


# Resources - Exports
class DataexportsView(MethodView):

    def search(self):
        global db
        _init()
        return ops.get_operation(request.base_url, db)

    def post(self):
        global db
        _init()
        ret1, ret2 = ops.post_operation(request, db)
        if ret1 is None:
            abort(400, ret2)
        return {'message': 'item added', "id": ret2}, 201

    def get(self, dataexport_id):
        global db
        _init()
        entry = ops.get_one_operation(request.base_url, dataexport_id, db)
        if entry is None:
            abort(404, "Entry with id " + dataexport_id + " was not found")
        return entry, 200

    def put(self, dataexport_id):
        global db
        _init()
        ret1, ret2 = ops.put_operation(request, dataexport_id, db)
        if ret1 is None:
            abort(404, ret2)
        return {'message': 'item put', "id": dataexport_id}, 200

    def patch(self, dataexport_id):
        global db
        _init()
        updatedEntry = ops.patch_operation(request, dataexport_id, db)
        if updatedEntry is None:
            abort(404, "Entry with id " + dataexport_id + " was not found")
        return {'message': 'item patched', "id": dataexport_id}, 200

    def delete(self, dataexport_id):
        global db
        _init()
        ret = ops.delete_operation(request.base_url, dataexport_id, db)
        if ret is None:
            abort(404, "Entry was not found")
        return {'message': 'item deleted'}, 200


def _init():
    global db
    db = setup_db_access()
