#! /usr/bin/env python

from flask import request, abort
from flask.views import MethodView
from connexion import NoContent
import libs.common_ops as ops
from libs.utilities import setup_db_access, get_type  # , load_entries

db = None


# Resources - Transformations
class DatatransformationsView(MethodView):

    def search(self):
        global db
        _init()
        return ops.get_operation(request.base_url, db)

    def post(self):
        global db
        _init()
        ret1, ret2 = ops.post_operation(request, db)
        if ret1 is None:
            abort(400, ret2)
        return {'message': 'item added', "id": ret2}, 201

    def get(self, datatransformation_id):
        global db
        _init()
        entry = ops.get_one_operation(request.base_url, datatransformation_id, db)
        if entry is None:
            abort(404, "Entry was not found")
        return entry, 200

    def put(self, datatransformation_id):
        global db
        _init()
        ret1, ret2 = ops.put_operation(request, datatransformation_id, db)
        if ret1 is None:
            abort(404, ret2)
        return {'message': 'item put', "id": datatransformation_id}, 200

    def patch(self, datatransformation_id):
        global db
        _init()
        updatedEntry = ops.patch_operation(request, datatransformation_id, db)
        if updatedEntry is None:
            abort(404, "Entry was not found")
        return {'message': 'item patched', "id": datatransformation_id}, 200

    def delete(self, datatransformation_id):
        global db
        _init()
        ret = ops.delete_operation(request.base_url, datatransformation_id, db)
        if ret is None:
            abort(404, "Entry was not found")
        # unload it
        return {'message': 'item deleted'}, 200


def _init():
    global db
    if db is None:
        db = setup_db_access()
