#! /usr/bin/env python

import os
import jwt
import json
from flask import request, abort
from flask.views import MethodView
from connexion import NoContent
from libs.const import UPLOAD_FOLDER, MAX_CONTENT_LENGTH, DATASRC_PUSH, DATASRC_FB, SERVER_KEY
import libs.common_ops as ops
from werkzeug.utils import secure_filename
from libs.utilities import setup_db_access, toJSONfile, allowed_file, create_folder, get_type, load_module

db = None


class GetAuthUrl():

    def get_auth_url(request, datasource_id, redirect_uri):
        global db
        _init()
        typeEntry = get_type(request.base_url)
        datasrc_entry = db.get_one(typeEntry, datasource_id)
        if datasrc_entry is None or not datasrc_entry.get('enabled'):
            abort(400, "Requested datasource entry does not exist or not enabled")
        elif (datasrc_entry.get('class') != "datasource-truelayer"):
            abort(400, "Not permitted action for such datasource")
        else:
            pass
        module = load_module(db.get_one(typeEntry, datasource_id))
        auth_url = module.get_auth_url(redirect_uri)
        return {"auth_url": auth_url}


class InitOauth2():

    def init_oauth2(request, body, datasource_id):
        global db
        _init()
        typeEntry = get_type(request.base_url)
        datasrc_entry = db.get_one(typeEntry, datasource_id)
        if datasrc_entry is None or not datasrc_entry.get('enabled'):
            abort(400, "Requested datasource entry does not exist or not enabled")
        elif (datasrc_entry.get('class') != "datasource-truelayer"):
            abort(400, "Not permitted action for such datasource")
        else:
            if (datasrc_entry.get('class') == DATASRC_PUSH) and 'redirect_uri' not in body:
                abort(400, "redirect_uri is missing")
            if (datasrc_entry.get('class') == DATASRC_PUSH) and 'access_code' not in body:
                abort(400, "access_code is missing")

        module = load_module(db.get_one(typeEntry, datasource_id))

        try:
            module.init_oauth2(db, datasource_id, body['redirect_uri'], body['access_code'])
        except Exception:
            return {"message": "Could not authenticate with TrueLayer."}

        return {"message": "ok"}


class DatasourceUpload():

    def upload(request, body, datasource_id):
        global db
        _init()
        typeEntry = get_type(request.base_url)
        datasrc_entry = db.get_one(typeEntry, datasource_id)
        if datasrc_entry is None or not datasrc_entry.get('enabled'):
            abort(400, "Requested datasource entry does not exist or not enabled")
        elif (datasrc_entry.get('class') != DATASRC_FB and datasrc_entry.get('class') != DATASRC_PUSH):
            abort(400, "Not permitted action for such datasource")
        else:
            if (datasrc_entry.get('class') == DATASRC_PUSH) and 'access_token' not in body:
                abort(400, "Access token is missing")
            if (datasrc_entry.get('class') == DATASRC_PUSH) and not _verifyJWT(datasrc_entry, body['access_token']):
                abort(400, "You are not allowed to have access")
            filename = None
            if 'file' in request.files:
                file = request.files['file']
                if file and allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    create_folder(UPLOAD_FOLDER)
                    file.save(os.path.join(UPLOAD_FOLDER, filename))
            elif "data" in body:
                create_folder(UPLOAD_FOLDER)
                filename = os.path.join(UPLOAD_FOLDER, datasource_id + "_uploaded.json")
                toJSONfile(filename, body["data"])
            else:
                abort(404, "Filetype not allowed or Data not found")
        module = load_module(datasrc_entry)
        ids = module.storeToDB(db, datasource_id, filename)
        if ids is not None:
            num = 1
            if type(ids) == list:
                num = len(ids)
            return {"message": "File uploaded and " + str(num) + " element(s) stored in the DB with key "+datasource_id}, 200
        else:
            return {"message": "No data stored in the DB with key " + datasource_id}, 404


def _verifyJWT(datasrc_entry, auth_token):
    """
    Decodes the auth token
    :param auth_token:
    :return: integer|string
    """
    print(auth_token)
    try:
        payload = jwt.decode(auth_token, SERVER_KEY)
        return payload['sub']
    except jwt.ExpiredSignatureError:  # Signature expired.
        print('Signature expired.')
        return False
    except jwt.InvalidTokenError:  # Invalid token. Please log in again.
        print('Invalid token. Please log in again.')
        return False


# Resources - DataSource
class DatasourcesView(MethodView):

    def search(self):
        global db
        _init()
        return ops.get_operation(request.base_url, db)

    def post(self):
        global db
        _init()
        module, datasource_id = ops.post_operation(request, db)
        if module is None:
            abort(404, datasource_id)
        return _JWTneeded(db, module, datasource_id)

    def get(self, datasource_id):
        global db
        _init()
        entry = ops.get_one_operation(request.base_url, datasource_id, db)
        if entry is None:
            abort(404, "Entry was not found")
        return entry, 200

    def put(self, datasource_id):
        global db
        _init()
        module, datasource_id = ops.put_operation(request, datasource_id, db)
        if module is None:
            abort(404, datasource_id)
        return _JWTneeded(db, module, datasource_id)

    def patch(self, datasource_id):
        global db
        _init()
        module, datasource_id = ops.patch_operation(request, datasource_id, db)
        if module is None:
            abort(404, datasource_id)
        return _JWTneeded(db, module, datasource_id)

    def delete(self, datasource_id):
        global db
        _init()
        ret = ops.delete_operation(request.base_url, datasource_id, db)
        if ret is None:
            abort(404, "Entry was not found")
        return {'message': 'item deleted'}, 200


def _JWTneeded(db, module, datasource_id):
    typeEntry = get_type(request.base_url)
    datasrc_entry = db.get_one(typeEntry, datasource_id)
    if datasrc_entry.get('class') == DATASRC_PUSH:
        token = module.encode_auth_token()
        db.update_one(typeEntry, datasource_id, {'$set': {"configuration": {"access_token": token}}})
        return {'message': 'operation successfull', "id": datasource_id, "access_token": token}, 200
    else:
        return {'message': 'operation successfull', "id": datasource_id}, 200


def _init():
    global db
    if db is None:
        db = setup_db_access()
