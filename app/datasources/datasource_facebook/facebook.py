#! /usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os.path


class FacebookDataStore:
    def __init__(self, name, configuration):
        pass  # do nothing

    def storeToDB(self, db, rid, filename):
        self.__loadData(filename)
        if self.data is None:
            print("> ERROR: No data loaded")
            return None
        else:
            if not db.get_all(rid) is None:
                db.delete_all(rid)
            if type(self.data) == list:
                return db.create_entries(rid, self.data)
            else:
                return db.create_entry(rid, self.data)

    def __loadData(self, datapath):
        if datapath is None or not os.path.isfile(datapath):
            self.data = None
        else:
            with open(datapath) as f:
                self.data = json.load(f)
