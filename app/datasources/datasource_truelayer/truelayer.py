from .truelayer_api import TrueLayerAPI
from datetime import datetime, timedelta, timezone


class TrueLayerDataStore:

    def __init__(self, name, configuration):
        client_id = configuration.get('client-id')
        client_secret = configuration.get('client-secret')
        print("TRUELAYER", client_id, client_secret)
        self.api = TrueLayerAPI(client_id, client_secret)

    def get_auth_url(self, redirect_uri):
        return self.api.get_auth_url(redirect_uri)

    def init_oauth2(self, db, rid, redirect_uri, access_code):

        self.api.exchange_code_for_token(redirect_uri, access_code)
        # you are authenticated now

        self.download_transactions(db, rid)

    def download_transactions(self, db, rid, back_hours=24):

        # choose the first account
        first_account = self.api.list_accounts()[0]

        # choose from/to params
        now = datetime.now(timezone.utc)
        to_date = now
        from_date = now - timedelta(hours=back_hours)

        # get transactions
        transactions = self.api.retrieve_transactions(first_account['account_id'], from_date, to_date)

        # add data to the database
        if not db.get_all(rid) is None:
            db.delete_all(rid)
        if type(self.data) == list:
            return db.create_entries(rid, transactions)
        else:
            return db.create_entry(rid, transactions)
