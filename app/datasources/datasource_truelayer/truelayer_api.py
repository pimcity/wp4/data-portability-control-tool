import time
import urllib

import requests

from datetime import datetime


class TrueLayerAPI:

    def __init__(self, client_id, client_secret):
        self.client_id = client_id
        self.client_secret = client_secret

    def get_auth_url(self, redirect_uri):

        query = urllib.parse.urlencode({
            'response_type': 'code',
            'response_mode': 'form_post',
            'client_id': self.client_id,
            'scope': 'accounts balance offline_access',
            'nonce': int(time.time()),
            'redirect_uri': redirect_uri,
            'enable_mock': 'true',
        })

        auth_uri = f'https://auth.truelayer-sandbox.com/?{query}'
        return auth_uri

    def exchange_code_for_token(self, redirect_uri, access_code):

        body = {
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'code': access_code,
            'grant_type': 'authorization_code',
            'redirect_uri': redirect_uri,
        }

        res = requests.post('https://auth.truelayer-sandbox.com/connect/token', data=body)

        # blow up if cannot retrive balance for an account
        res.raise_for_status()

        self.tokens = res.json()
        # done, you are authenticated

    def _get_auth_header(self):
        access_token = self.tokens['access_token']
        auth_header = {'Authorization': f'Bearer {access_token}'}
        return auth_header

    def list_accounts(self):

        auth_header = self._get_auth_header()
        res = requests.get('https://api.truelayer.com/data/v1/accounts', headers=auth_header)

        # blow up if cannot retrive balance for an account
        res.raise_for_status()

        return res.json()['results']

    def retrieve_balance(self, account_id):

        auth_header = self._get_auth_header()
        res = requests.get(f'https://api.truelayer-sandbox.com/data/v1/accounts/{account_id}/balance', headers=auth_header)

        # blow up if cannot retrive balance for an account
        res.raise_for_status()

        return res.json()['results']

    def retrieve_transactions(self, account_id, from_date, to_date=None):

        if to_date is None:
            to_date = datetime.now()

        # iso format dates
        from_date = from_date.isoformat()
        to_date = to_date.isoformat()

        auth_header = self._get_auth_header()
        res = requests.get(f'https://api.truelayer-sandbox.com/data/v1/accounts/{account_id}/transactions?from={from_date}&to={to_date}', headers=auth_header)

        # blow up if cannot retrive balance for an account
        res.raise_for_status()

        return res.json()['results']
