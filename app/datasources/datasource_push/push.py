#! /usr/bin/env python
# -*- coding: utf-8 -*-

import jwt, json, os, datetime
from libs.const import SERVER_KEY
import os.path


class PushDataStore:        
    def __init__(self, name, configuration):
        pass #do nothing

    def storeToDB(self, db, rid, filename):
        self.__loadData(filename)
        if self.data is None:
            print("> ERROR: No data loaded")
            return None
        else: #append
            if type(self.data) == list:
                return db.create_entries(rid, self.data)
            else:
                return db.create_entry(rid, self.data)

    def __loadData(self, datapath):
        print("Trying to load data from",datapath)
        if datapath is None or not os.path.isfile(datapath):
            self.data = None
        else:
            with open(datapath) as f:
                self.data = json.load(f)

    def encode_auth_token(self):
        """
        Generates the Auth Token
        :return: string
        """
        try:
            payload = {
                'iat': datetime.datetime.utcnow(),
            }
            return jwt.encode(
                payload,
                SERVER_KEY,
                algorithm='HS256'
            )
        except Exception as e:
            return e

