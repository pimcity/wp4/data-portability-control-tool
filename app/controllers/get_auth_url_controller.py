import connexion
from api.datasourcesview import GetAuthUrl


def get_auth_url(datasource_id, redirect_uri):
    return GetAuthUrl.get_auth_url(connexion.request, datasource_id, redirect_uri)
