from keycloak import KeycloakOpenID

"""
controller generated to handled auth operation described at:
https://connexion.readthedocs.io/en/latest/security.html
"""


def check_OAuth2(token):
    keycloak_openid = KeycloakOpenID(
        server_url="https://easypims.pimcity-h2020.eu/identity/auth/",
        client_id="dpc",
        realm_name="pimcity",
    )

    KEYCLOAK_PUBLIC_KEY = (
        "-----BEGIN PUBLIC KEY-----\n"
        + keycloak_openid.public_key()
        + "\n-----END PUBLIC KEY-----"
    )
    options = {
        "verify_signature": True,
        "verify_aud": True,
        "verify_exp": True,
        "audience": "dpc",
    }
    token_info = keycloak_openid.decode_token(
        token, key=KEYCLOAK_PUBLIC_KEY, options=options
    )

    scope = token_info["scope"].split()

    return {"scopes": scope}


def validate_scope_OAuth2(required_scopes, token_scopes):
    return set(required_scopes).issubset(set(token_scopes))
