import connexion
from api.datasourcesview import InitOauth2


def init_oauth2(body, datasource_id):
    return InitOauth2.init_oauth2(connexion.request, body, datasource_id)
