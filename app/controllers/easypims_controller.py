import connexion
import requests
import json

from flask import abort
from datetime import datetime
from urllib.parse import urlparse
from libs.utilities import setup_db_access

# global dict for usename to ids mapping
# TODO: storing it in mongo makes more sense
#usernames_dict = {}

db = None

def _init():
    global db
    if db is None:
        db = setup_db_access()


def init_user(body):
    global db
    _init()
    # get username
    username = body['username']

    # inti user info
    user_info = {}

    # create a new FB datasource
    datasource_id = create_new_fb_datasource()

    # save the id
    user_info['datasource_id'] = datasource_id

    # enable the new datasource
    enable_datasource(datasource_id)

    # TODO: Optionally create and configure a transformation
    # ...

    # create a new data export (to PDS)
    dataexport_id = create_new_pds_export()

    # save the id
    user_info['dataexport_id'] = dataexport_id

    if db.get_name('clients', username) is not None:
        abort(400, f"Error: A different entry with the same name already exists")
    db.create_entry('clients', {"name": username, "mod_ids": user_info})
    print("DATABASE INCLUDES:",db.get_all("clients"))
    
    # return reponse
    response = {
        "submit_fb_data_url": f"/datasources/{datasource_id}/upload",
        "send_data_url": f"/dataexports/{dataexport_id}/",
    }

    return response


def delete_user(body):
    global db
    _init()

    # get username
    username = body['username']
    print("BEFORE DELETE DATABASE INCLUDES:",db.get_all("clients"))

    client = db.get_name('clients', username)
    if client is None:
        abort(400, f"user with username '{username}' has not been initialized in EasyPims")
    # get userinfo
    user_info = client["mod_ids"]

    # send data to PDS
    dataexport_id = user_info['dataexport_id']
    # export_to_pds(dataexport_id)  # TODO: Enable when Export is implemented

    # delete datasources and dataexports
    datasource_id = user_info['datasource_id']
    delete_datasource(datasource_id)
    delete_dataexport(dataexport_id)

    db.delete_one('clients', client["_id"])
    print("AFTER DELETE DATABASE INCLUDES:",db.get_all("clients"))
    return "Deleted"

def create_new_fb_datasource():

    url = _get_server() + '/datasources'
    data = {
        "class": "datasource-facebook",
        "manifest-version": 1,
        "name": datetime.now().isoformat(),
        "type": "datasources",
    }

    response = requests.post(url, data=json.dumps(data), headers=_get_headers()).json()

    return response['id']


def enable_datasource(datasource_id):

    url = _get_server() + f'/datasources/{datasource_id}'
    data = {
        "enabled": True,
    }

    requests.patch(url, data=json.dumps(data), headers=_get_headers())


def create_new_pds_export():

    url = _get_server() + '/dataexports'
    data = {
        "class": "dataexport-pds",
        "manifest-version": 1,
        "name": datetime.now().isoformat(),
        "type": "dataexports",
    }

    response = requests.post(url, data=json.dumps(data), headers=_get_headers()).json()

    return response['id']


def export_to_pds(dataexport_id):

    url = _get_server() + f'/dataexports/{dataexport_id}'
    data = {
        "enabled": True,
    }

    requests.patch(url, data=json.dumps(data), headers=_get_headers())


def delete_datasource(datasource_id):

    url = _get_server() + f'/datasources/{datasource_id}'
    requests.delete(url, headers=_get_headers())


def delete_dataexport(dataexport_id):

    url = _get_server() + f'/dataexports/{dataexport_id}'
    requests.delete(url, headers=_get_headers())


def _get_server():
    base_url = connexion.request.base_url
    result = urlparse(base_url)
    return f"{result.scheme}://{result.netloc}"


def _get_headers():
    return connexion.request.headers
