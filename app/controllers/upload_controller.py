import connexion
from api.datasourcesview import DatasourceUpload


def datasource_upload(body, datasource_id):  # noqa: E501
    """ Creates upload stream for datasource

    :param body:
    :type body: dict | bytes
    :param datasource_id: id of datasource to populate
    :type name: string

    :rtype: None
    """
    return DatasourceUpload.upload(connexion.request, body, datasource_id)
