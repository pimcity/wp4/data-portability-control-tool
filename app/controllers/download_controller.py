import connexion
from api.dataexportsview import DataexportDownload


def dataexport_download(dataexport_id):  # noqa: E501
    """ Creates down stream for dataexport

    :param body:
    :type body: dict | bytes
    :param dataexport_id: id of datasource to populate
    :type name: string

    :rtype: None
    """
    return DataexportDownload.download(connexion.request, dataexport_id)
